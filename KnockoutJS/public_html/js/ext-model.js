function Task(title, done) {
    this.title = ko.observable(title);
    this.done = ko.observable(done);
}

function ExtModel() {
    this.list = ko.observableArray(['Anna', 'Magda', 'Monika', 'Kasia', 'Ola']);
    this.tasks = ko.observableArray([
        new Task('Task 1', true),
        new Task('Task 2', false),
        new Task('Task 3', true)
    ]);
    this.doneTasks = this.tasks.filterByProperty("done", true);
    this.someText = ko.observable('Text...').extend({logChange: "var someText"});
}

// extension
ko.bindingHandlers.happy = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        // This will be called when the binding is first applied to an element
        // Set up any initial state, event handlers, etc. here
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        // This will be called once when the binding is first applied to an element,
        // and again whenever any observables/computeds that are accessed change
        // Update the DOM element based on the supplied values here.
        var smiles = [' :-)', ' :->', ' ;-)'];

        var values = ko.unwrap(valueAccessor());
        var html = element.innerHTML;
        element.innerHTML = '';
        for (var i in values) {
            element.innerHTML += html.replace('$value', '\'' + values[i] + smiles[values[i].charCodeAt(0) % 3] + '\'');
        }
    }
};

ko.components.register('my-tag', {
    viewModel: function (params) {
        this.firstname = ko.observable(params.firstname || '');
        this.lastname = ko.observable(params.lastname || '');
    },
    template: '<div class="text-info" data-bind="text: firstname() + \' \' + lastname()"></div>'
});

ko.observableArray.fn.filterByProperty = function (propName, matchValue) {
    return ko.pureComputed(function () {
        var allItems = this(), matchingItems = [];
        for (var i = 0; i < allItems.length; i++) {
            var current = allItems[i];
            if (ko.unwrap(current[propName]) === matchValue)
                matchingItems.push(current);
        }
        return matchingItems;
    }, this);
};

ko.extenders.logChange = function (target, option) {
    target.subscribe(function (newValue) {
        console.log(option + ": " + newValue);
    });
    return target;
};

var container = document.getElementById('mainContainer');
ko.applyBindings(new ExtModel(), container);
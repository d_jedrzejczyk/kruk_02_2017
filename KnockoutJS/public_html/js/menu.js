function Tab(label, path) {
    this.label = label;
    this.path = path;
}

function MenuModel() {
    var self = this;
    self.tabs = ko.observableArray([
        new Tab('Data binding', 'index.html'),
        new Tab('Szablony', 'templates.html'),
        new Tab('Rozszerzalność', 'extensibility.html'),
        new Tab('Obserwacja zmiennych', 'listeners.html'),
        new Tab('Dokumentacja', 'docs.html')
    ]);

    self.isActive = function (tab) {
        return document.location.pathname === '/KnockoutJS/' + tab.path;
    };
}

var menuContainer = document.getElementById('menu');
ko.applyBindings(new MenuModel(), menuContainer);
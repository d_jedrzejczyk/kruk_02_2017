function MainModel() {
    this.firstName = ko.observable('Jan');
    this.lastName = ko.observable('Kowalski');
}

var container = document.getElementById('mainContainer');
ko.applyBindings(new MainModel(), container);
function ListenersModel() {
    var self = this;

    this.input = ko.observable('');
    this.successFlag = ko.observable(false);

    this.input.subscribe(function (newValue) {
        self.successFlag(newValue === 'success');
    });
}

var container = document.getElementById('mainContainer');
ko.applyBindings(new ListenersModel(), container);
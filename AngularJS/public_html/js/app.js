angular.module('app', ['ngRoute']).config(function ($routeProvider) {
    $routeProvider.when('/databinding', {
        templateUrl: 'parts/databinding.html',
        controller: 'AppController'
    }).when('/templates', {
        templateUrl: 'parts/templates.html'
    }).when('/listeners', {
        templateUrl: 'parts/listeners.html',
        controller: 'ListenersController'
    }).when('/extensibility', {
        templateUrl: 'parts/extensibility.html',
        controller: 'ExtController'
    }).when('/docs', {
        templateUrl: 'parts/docs.html'
    }).otherwise('/databinding');
}).run(function ($rootScope, $location) {
    $rootScope.tabs = [
        new Tab('Data binding', 'databinding'),
        new Tab('Szablony', 'templates'),
        new Tab('Rozszerzalność', 'extensibility'),
        new Tab('Obserwacja zmiennych', 'listeners'),
        new Tab('Dokumentacja', 'docs')
    ];

    $rootScope.isActive = function (tab) {
        return $location.path() === '/' + tab.path;
    };
});

function Tab(label, path) {
    this.label = label;
    this.path = path;
}
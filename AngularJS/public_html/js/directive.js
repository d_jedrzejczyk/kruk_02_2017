angular.module('app').directive('myTag', function () {
    return {
        restrict: 'AECM',
        scope: {firstname: '@firstname', lastname: '@lastname'},
        template: '<div class="text-info">{{firstname}} {{lastname}}</div>'
    };
});
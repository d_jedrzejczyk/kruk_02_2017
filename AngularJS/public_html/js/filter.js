angular.module('app').filter('happy', function () {
    return function (input) {
        var smiles = [' :-)', ' :->', ' ;-)'];
        var out = [];
        for (var i = 0; i < input.length; i++) {
            out.push(input[i] + smiles[input[i].charCodeAt(0) % 3]);
        }
        return out;
    };
});
angular.module('app').controller('ListenersController', function ($scope) {
    $scope.input = '';
    $scope.successFlag = false;
    $scope.$watch('input', function (newValue, oldValue) {
        $scope.successFlag = newValue === 'success';
    });
});